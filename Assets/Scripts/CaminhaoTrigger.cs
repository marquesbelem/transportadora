﻿using UnityEngine;

public class CaminhaoTrigger : MonoBehaviour {

    bool destino;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("Carga")) {
            if (!destino)
                Controlador._intance.CarregarCarga();
        }

        if (!coll.gameObject.CompareTag("Carga") && !coll.gameObject.CompareTag("DestinoMissaoTransportadora")
            && !coll.gameObject.CompareTag("Player") && coll.gameObject.CompareTag("Untagged")) {
            if (!destino)
                Controlador._intance.Batidas();
        }

        if (coll.gameObject.CompareTag("DestinoMissaoTransportadora"))  {
            destino = true;
            Controlador._intance.Destino();
        }
    }
}
