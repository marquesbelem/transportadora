﻿/*
 Esse codigo é usado para controlar todo o mini game 

 Largada = Objeto do caminhão
 Destino = Objeto ponto B 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controlador : MonoBehaviour {

    public static Controlador _intance = null;

    [SerializeField]
    GameObject box_iniciar;
    
    [SerializeField]
    GameObject largada, destino, carga, iniciar_missao;
    public bool dentro_caminhao, caminhao_carga;

    public bool inicia_cronometro;
    float cronometro = 0;
    public Text text_cronometro;

    int batidas = 0;
    public Text text_batidas;

    void Awake()
    {
        if (_intance == null)
            _intance = this;
    }

    void Start ()
    {
        box_iniciar.SetActive(false);
        largada.SetActive(false);
        destino.SetActive(false);
        dentro_caminhao = false;

        text_cronometro.gameObject.SetActive(false);
        text_cronometro.text = "Tempo : " + cronometro.ToString("00");
        inicia_cronometro = false;

        text_batidas.gameObject.SetActive(false);
        text_batidas.text = "Batidas: " + batidas;
    }
	
	
	void Update ()
    {
        if (caminhao_carga)
            CarregarCarga();

        if (inicia_cronometro)
            Cronometro();
    }

   public void IniciarMissao() {
       box_iniciar.SetActive(true);
   }

   public void IniciarMissaoSim()
   {
        largada.SetActive(true);
        destino.SetActive(true);
        box_iniciar.SetActive(false);
        iniciar_missao.SetActive(false);
        text_cronometro.gameObject.SetActive(true);
        text_batidas.gameObject.SetActive(true);
    }

    public void IniciarMissaoNao() {
        box_iniciar.SetActive(false);
        iniciar_missao.SetActive(true);
        inicia_cronometro = false;
    }

    public void CarregarCarga() {
        Debug.Log("Carregou o caminho com a carga");
        caminhao_carga = true;
        carga.transform.position = largada.transform.position;
    }

    public void Destino() {
        Debug.Log("Descarregou a carga");
        caminhao_carga = false;
        inicia_cronometro = false;
        carga.transform.position = destino.transform.position;
    }

    void Cronometro() {
        cronometro += Time.deltaTime;
        text_cronometro.text = "Tempo: " + cronometro.ToString("00");
    }

    public void Batidas()
    {
        batidas++;
        text_batidas.text = "Batidas: " + batidas;
    }

}
