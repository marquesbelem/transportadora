﻿
using UnityEngine;

public class PlayerTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("IniciarMissaoTransportadora")) {
            Controlador._intance.IniciarMissao();
        }
        if (coll.gameObject.CompareTag("Caminhao"))  {
            Controlador._intance.dentro_caminhao = true;
            Controlador._intance.inicia_cronometro = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.CompareTag("Caminhao"))  {
            Controlador._intance.dentro_caminhao = false;
            Controlador._intance.inicia_cronometro = false;
        }
    }
}
